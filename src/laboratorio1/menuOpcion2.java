/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio1;

import java.util.Arrays;

/**
 *
 * @author usuario
 */
public class menuOpcion2 {

    /*
    
    Construye e imprime la tabla de posiciones
    
    
    Esta parte ya la tengo, en matrizequipos.
    La tabla de posiciones se construye a partir de que se van realizando los juegos,
    en donde se va actualizando los goles a favor, en contra y los puntos obtenidos. 
    
    
    b.	Cuando la tabla de posiciones se imprime, debe considerar los siguientes criterios para acomodar los equipos: 
    
•	El primer criterio lo determina la cantidad de puntos que tenga cada equipo; entre más puntos tenga, mejor posicionado en la tabla. 
•	En caso de que dos o más equipos tengan igualdad de puntos, se debe valorar quién tiene más goles a favor; entre más tenga, mejor posicionado en la tabla. 
•	En caso de que dos o más equipos igualen en goles a favor, se tomará como criterio el que tenga menos goles en contra; entre menos tenga, mejor posicionado en la tabla. 

    
    
     */
    public void imprimirTablaPosiciones() {
        Equipo[] arrayEquipos = new Equipo[6];
        for (int c = 0; c < Laboratorio1.matrizequipos.length; c++) {
            arrayEquipos[c] = new Equipo((String) Laboratorio1.matrizequipos[c][0], (int) Laboratorio1.matrizequipos[c][1], (int) Laboratorio1.matrizequipos[c][2], (int) Laboratorio1.matrizequipos[c][3]);
        }
        Arrays.sort(arrayEquipos);
        System.out.println("|EQUIPO|GF|GC|PTS");
        imprimirArrayEquipos(arrayEquipos);

    }

    static void imprimirArrayEquipos(Equipo[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println("|" + array[i].equipo + "|" + array[i].gf + "|" + array[i].gc + "|" + array[i].equi);
        }
    }

    public class Futbol {

    }

    static class Equipo implements Comparable<Equipo> {

        public String equipo;
        public int gf;
        public int gc;
        public int equi;

        public Equipo(String equipo, int gf, int gc, int equi) {
            this.equipo = equipo;
            this.gf = gf;
            this.gc = gc;
            this.equi = equi;

        }

        @Override
        public int compareTo(Equipo e) {// lo que hicimos fue un compareTo, que lo que hace es comparar lo valores de mi informacion,
            Integer a = this.equi;// lo que hacemos es optener el equipo
            Integer b = e.equi;
            if (b.compareTo(a) == 0) {
                Integer x = this.gf;//compararlocon goles a favor
                Integer y = e.gf;
                if (y.compareTo(x) == 0) {
                    Integer l = this.gc;//goles en contra.
                    Integer d = e.gc;
                    return l.compareTo(d);

                } else {

                    return y.compareTo(x);// utilizo los retornos.

                }

            } else {
                return b.compareTo(a);
            }
        }
    }

}
