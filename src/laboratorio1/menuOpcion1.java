/*
 El sistema debe tomar los 6 equipos y conformar parejas de enfrentamientos de manera aleatoria; 
debe considerar que, las parejas deben ser entre equipos distintos y un equipo no puede enfrentarse a más de 2 adversarios; 
por ejemplo: LDA vs LDA no se podría, o LDA vs San Carlos y LDA vs Saprissa. Entonces, cada vez que se acceda a la opción 1 del menú, 
se conformarán 3 emparejamientos.



 */
package laboratorio1;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author usuario
 */
public class menuOpcion1 {

    public void crearenfrentamiento() {

        Random randon = new Random();
        ArrayList listaequipos = new ArrayList<>();
        listaequipos.add("LDA");
        listaequipos.add("Saprissa");
        listaequipos.add("Herediano");
        listaequipos.add("San Carlos");
        listaequipos.add("Limón");
        listaequipos.add("Cartago");
        for (int i = 0; i < 3; i++) {//este for es para sacar dos equipos de la lista.
            int tomarran = randon.nextInt(listaequipos.size());
            String primerequipo = (String) listaequipos.get(tomarran);//lo del primerequipo es para tomar el primer equipo
            listaequipos.remove(tomarran);//no se hace validacion, porque se remueve con el remove.
            int segundtomarran = randon.nextInt(listaequipos.size());
            String segunodrequipo = (String) listaequipos.get(segundtomarran);//en esta parte ese toma el segundo equipo
            listaequipos.remove(segundtomarran);//no se hace validacion, porque se remueve con el remove.

            /*
                Una vez que están creados los enfrentamientos, la manera de jugar el partido es generando un número aleatorio entre 0 y 5 
para determinar cuántos goles anota el equipo 1 y cuántos el equipo 2, para conocer el ganador; gana quién anote más goles. 
         
                
             */
 /*

        Si alguno de los equipos resulta ganador en la partida inicial (ver punto B) obtiene 3 puntos;
        en caso de que resulte empate y se hagan los lanzamientos de penal (ver punto C),
        el equipo ganador obtiene 1 punto; y, en ambos casos (puntos B y C) el equipo que resulte perdedor no
        obtiene puntos. 
             */
            String Egano = null;
            String Eperdi = null;
            int GolesContraP = 0;
            int GolesFavorP = 0;
            int GolesAfavorG = 0;
            int GolesContrG = 0;
            int puntos = 0;

            int primergol = randon.nextInt(5);//de esta forma generamos lo randoms para asignar los goles.
            int segundogol = randon.nextInt(5);
            System.out.println("______________________________________________");
            System.out.println(primerequipo + " Vrs " + segunodrequipo);
            System.out.println(primergol + "           " + segundogol);
            if (primergol > segundogol) {
                Egano = primerequipo;//Esta parte es para ir obteniendo lo de los puntos.
                Eperdi = segunodrequipo;
                GolesAfavorG = primergol;
                GolesContrG = segundogol;
                GolesFavorP = segundogol;
                GolesContraP = primergol;
                puntos = 3;
                System.out.println(primerequipo + " 3 PTS " + segunodrequipo + " 0 PTS");

                System.out.println("El ganador es " + primerequipo);
                java.util.Date fecha = new Date();
                System.out.println("Fecha "+fecha);

            } else if (segundogol > primergol) {
                Egano = segunodrequipo;//Esta parte es para ir obteniendo lo de los puntos.
                Eperdi = primerequipo;
                GolesAfavorG = segundogol;
                GolesContrG = primergol;
                GolesFavorP = primergol;
                GolesContraP = segundogol;
                puntos = 3;
                System.out.println(segunodrequipo + " 3 PTS " + primerequipo + " 0 PTS");

                System.out.println("El ganador es " + segunodrequipo);
                java.util.Date fecha = new Date();
                System.out.println("Fecha "+fecha);
            } else if (segundogol == primergol) {

                /*
            
            
            En caso de que exista un empate en goles, debe generarse un desempate a través de lanzamiento de penales, 
            en donde un equipo será el que lance y el otro deba hacer la labor de atajar los penales (ud elige cuál cumple cada función). 
            En cuanto al desempate funciona de la siguiente manera: consiste en 5 lanzamientos por parte de los jugadores que patean el balón, 
            los cuales pueden decidir lanzar en cualquiera de las seis secciones del arco: 
            
            
            
        arriba a la derecha (1) 
•	arriba al centro (2) 
•	arriba a la izquierda (3) 
•	abajo a la izquierda (4) 
•	abajo al centro (5) 
•	abajo a la derecha (6) 

            
                 */
                System.out.println("Empate");
                System.out.println("El equipo " + primerequipo + " es el que va a tirar los penales.");
                Scanner ingreso = new Scanner(System.in);
                System.out.println("________________________________________________________");
                System.out.println("|              |                      |                 |");
                System.out.println("|      3       |            2         |         1       |");
                System.out.println("|______________|______________________|_________________");
                System.out.println("|      4       |            5         |         6       |");
                System.out.println("|______________|______________________|_________________|");
                int penalesAtajados = 0;//estos for son para ir contado la cantidad de penales anotados y atajados.
                int penalesAnotados = 0;
                for (int t = 0; t < 5; t++) {

                    System.out.print("\nTiene 5 lanzamientos, digite la dirección donde quiere que vaya la bola: ");
                    int tiros = ingreso.nextInt();
                    int ranpenales = randon.nextInt(5);

                    if (tiros == ranpenales) {
                        penalesAtajados++;
                    } else if (tiros != ranpenales) {

                        penalesAnotados++;
                    }
                    if (penalesAtajados > penalesAnotados) {
                        System.out.println("El ganador es " + primerequipo);// Se imprime todas las veces, preguntar.

                        Egano = segunodrequipo;//Esta parte es para ir obteniendo lo de los puntos.
                        Eperdi = primerequipo;
                        GolesAfavorG = segundogol;
                        GolesContrG = primergol;
                        GolesFavorP = primergol;
                        GolesContraP = segundogol;
                        puntos = 1;

                    } else if (penalesAtajados < penalesAnotados) {

                        System.out.println("El ganador es " + segunodrequipo);
                        Egano = primerequipo;//Esta parte es para ir obteniendo lo de los puntos.
                        Eperdi = segunodrequipo;
                        GolesAfavorG = primergol;
                        GolesContrG = segundogol;
                        GolesFavorP = segundogol;
                        GolesContraP = primergol;
                        puntos = 1;

                    }

                }
            }
            for (int p = 0; p < Laboratorio1.matrizequipos.length; p++) {
                if (Egano == Laboratorio1.matrizequipos[p][0]) {

                    int GF = (int) Laboratorio1.matrizequipos[p][1];//De esta forma se estan leyendo la cantidad de goles que tienen.
                    int GC = (int) Laboratorio1.matrizequipos[p][2];
                    int PTS = (int) Laboratorio1.matrizequipos[p][3];// Puntos
                    
                    
                    int goles = (int) Laboratorio1.matrizequipos[p][4];//agregar los goles.
                    
                    
                    Laboratorio1.matrizequipos[p][1] = GF + GolesAfavorG;// en esta parte estamos guardando.
                    Laboratorio1.matrizequipos[p][2] = GC + GolesContrG;
                    Laboratorio1.matrizequipos[p][3] = PTS + puntos;
                    Laboratorio1.matrizequipos[p][4] = goles + 1;//e esta parte ese va agregando lo de los goles.
                    
                    
                    
                } else if (Eperdi == Laboratorio1.matrizequipos[p][0]) {
                    int GF = (int) Laboratorio1.matrizequipos[p][1];//De esta forma se estan leyendo la cantidad de goles que tienen.
                    int GC = (int) Laboratorio1.matrizequipos[p][2];
                    int PTS = (int) Laboratorio1.matrizequipos[p][3];// Puntos

                    Laboratorio1.matrizequipos[p][1] = GF + GolesFavorP;// en esta parte estamos guardando.
                    Laboratorio1.matrizequipos[p][2] = GC + GolesContraP;
                    Laboratorio1.matrizequipos[p][3] = PTS + 0;

                }

            }

        }

        //for (int p = 0; p < Laboratorio1.matrizequipos.length; p++) {
            //System.out.println(" ");
            //for (int v = 0; v < Laboratorio1.matrizequipos[p].length; v++) {
                //System.out.println(Laboratorio1.matrizequipos[p][v]);

            }

        }

        /*
    
Por cada ronda de partidos (es decir, los 3 partidos) se debe imprimir lo siguiente: 
- Nombres de los equipos: equipo1 vs equipo2.				
- Marcador: equipo1 “X” y equipo2 “Y”.
- Puntos: equipo1 “X” y equipo2 “Y”.
- Fecha: se toma la fecha del sistema. 

         */
    


