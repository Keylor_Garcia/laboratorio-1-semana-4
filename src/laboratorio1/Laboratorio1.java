package laboratorio1;

import java.util.Scanner;

/**
 *
 * @author usuario
 */
public class Laboratorio1 {

    
    public static Object[][] matrizequipos = {{"LDA", 0, 0, 0,0}, {"Saprissa", 0, 0, 0,0}, {"Herediano", 0, 0, 0,0}, {"San Carlos", 0, 0, 0,0}, {"Limón", 0, 0, 0,0}, {"Cartago", 0, 0, 0,0}};//se utilizo objeto para obtener numeros, se hizo con public static para poder llamarla en otra clase.

    /**
     * @param args the command line arguments
     *
     *
     *
     */

    public static void main(String[] args) {

        Scanner ingreso = new Scanner(System.in);
        int opcion;
        boolean salir = false;
        while (!salir) {

            /*
        En esta parte va a ir el menú
             */
            System.out.print("\n\nLista de programas\n1.Jugar\n2.Tabla de posiciones\n3.Reporte\n4.Salir\nOpción a ejecutar:  ");

            opcion = ingreso.nextInt();
            switch (opcion) {
                case 1:
                    menuOpcion1 datos1 = new menuOpcion1();
                    datos1.crearenfrentamiento();
                    break;

                case 2:
                    menuOpcion2 datos2 = new menuOpcion2();
                    datos2.imprimirTablaPosiciones();
                    break;
                case 3:
                   menuOpcion3 datos3 = new menuOpcion3();
                    datos3.ImpresionReporte();
                    break;

                case 4:
                    salir = true;
                    break;

                default:
                    System.out.printf("\nOpción no válida\n");
                    break;

            }
        }

    }
}
